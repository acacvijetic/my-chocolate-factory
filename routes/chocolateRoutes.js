const express = require('express');
const router = express.Router();
const ChocolateController = require('../controllers/chocolateController');

router.post('/chocolates', ChocolateController.createChocolate);
router.get('/chocolates/:id', ChocolateController.getChocolate);
router.delete('/chocolates/:id', ChocolateController.deleteChocolate);
router.put('/chocolates/:id', ChocolateController.updateChocolate);

router.get('/chocolates', ChocolateController.getAllChocolates);

module.exports = router;
