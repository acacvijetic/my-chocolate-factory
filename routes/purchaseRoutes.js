const express = require('express');
const router = express.Router();
const PurchaseController = require('../controllers/purchaseController');

// POST /purchases - Kreiranje nove kupovine
router.post('/purchases', PurchaseController.createPurchase);

// GET /purchases - Dobavljanje svih kupovina
router.get('/purchases', PurchaseController.getAllPurchases);

// GET /purchases/:id - Dobavljanje pojedinačne kupovine po ID-u
router.get('/purchases/:id', PurchaseController.getPurchase);

// DELETE /purchases/:id - Brisanje kupovine po ID-u
router.delete('/purchases/:id', PurchaseController.deletePurchase);

// PUT /purchases/:id - Ažuriranje kupovine po ID-u
router.put('/purchases/:id', PurchaseController.updatePurchase);

module.exports = router;
