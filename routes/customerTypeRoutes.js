const express = require('express');
const router = express.Router();
const CustomerTypeController = require('../controllers/customerTypeController');

router.post('/customerTypes', CustomerTypeController.createCustomerType);
router.get('/customerTypes/:ime_tipa', CustomerTypeController.getCustomerType);
router.delete('/customerTypes/:ime_tipa', CustomerTypeController.deleteCustomerType);
router.put('/customerTypes/:ime_tipa', CustomerTypeController.updateCustomerType);

module.exports = router;
