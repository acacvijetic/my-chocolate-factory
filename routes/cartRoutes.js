const express = require('express');
const router = express.Router();
const CartController = require('../controllers/cartController');

router.post('/carts', CartController.createCart);
router.get('/carts/:id', CartController.getCart);
router.get('/carts/username/:username', CartController.getCartByCustomer);
router.delete('/carts/:id', CartController.deleteCart);
router.put('/carts/:id', CartController.updateCart);

module.exports = router;
