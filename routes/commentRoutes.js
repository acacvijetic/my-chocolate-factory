const express = require('express');
const router = express.Router();
const CommentController = require('../controllers/commentController');

router.post('/comments', CommentController.createComment);
router.get('/comments', CommentController.getAllComments);
router.delete('/comments/:kupac/:fabrika', CommentController.deleteComment);
router.put('/comments/:id', CommentController.updateComment);

router.get('/comments/:fabrika', CommentController.getCommentsByFactoryName);

module.exports = router;
