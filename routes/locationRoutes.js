const express = require('express');
const router = express.Router();
const LocationController = require('../controllers/locationController');

router.post('/locations', LocationController.createLocation);
router.get('/locations/:geografska_duzina/:geografska_sirina', LocationController.getLocation);
router.delete('/locations/:geografska_duzina/:geografska_sirina', LocationController.deleteLocation);
router.put('/locations/:geografska_duzina/:geografska_sirina', LocationController.updateLocation);

module.exports = router;
