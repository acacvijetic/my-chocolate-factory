const express = require('express');
const router = express.Router();
const ChocolateFactoryController = require('../controllers/chocolateFactoryController');

router.post('/chocolateFactories', ChocolateFactoryController.createChocolateFactory);
router.get('/chocolateFactories/:id', ChocolateFactoryController.getChocolateFactory);
router.delete('/chocolateFactories/:naziv', ChocolateFactoryController.deleteChocolateFactory);
router.put('/chocolateFactories/:id', ChocolateFactoryController.updateChocolateFactory);

router.get('/chocolateFactories', ChocolateFactoryController.getAllChocolateFactories);
router.get('/chocolateFactories/name/:naziv', ChocolateFactoryController.getChocolateFactoryByName);


module.exports = router;