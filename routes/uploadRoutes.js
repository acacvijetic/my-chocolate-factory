// routes/uploadRoutes.js
const express = require('express');
const router = express.Router();
const uploadController = require('../controllers/uploadController');

router.post('/upload', uploadController.upload.single('image'), uploadController.uploadImage);

module.exports = router;
