const express = require('express');
const fs = require('fs');
const jwt = require('jsonwebtoken');
const router = express.Router();
const SECRET = 'your_jwt_secret'; // Replace with your actual secret
const UserController = require('../controllers/UserController');

router.get('/users', UserController.getAllUsers);

router.get('/users/:korisnickoIme', UserController.getUser);

router.post('/users', UserController.createUser);

router.put('/users/:korisnickoIme', UserController.updateUser);

// Route for user login
router.post('/users/login', (req, res) => {
  const { username, password } = req.body;

  // Load users from JSON file
  fs.readFile('data/korisnici.json', (err, data) => {
    if (err) return res.status(500).json({ message: 'Error reading user data' });

    const users = JSON.parse(data);
    const user = users.find(u => u.korisnickoIme === username && u.lozinka === password);

    if (!user) return res.status(400).json({ message: 'Invalid username or password' });

    const token = jwt.sign({ id: user.id, username: user.korisnickoIme, role: user.uloga }, SECRET, { expiresIn: '1h' });
    res.json({ token, user });
  });
});


router.post('/users/register', (req, res) => {
  const { username, password, firstName, lastName, gender, birthdate, role, factory } = req.body;

  // Load users from JSON file
  fs.readFile('data/korisnici.json', (err, data) => {
    if (err) return res.status(500).json({ message: 'Error reading user data' });

    const users = JSON.parse(data);
    
    // Check if user already exists
    const existingUser = users.find(u => u.korisnickoIme === username);
    if (existingUser) return res.status(400).json({ message: 'Username already exists' });

    // Generate new id as string
    const newId = (users.length + 1).toString();

    // Create new user object with string id
    const newUser = {
      id: newId,
      korisnickoIme: username,
      lozinka: password,
      ime: firstName,
      prezime: lastName,
      pol: gender,
      datumRodjenja: birthdate,
      uloga: role,
      sveKupovine: [],
      korpa: [],
      fabrikaCokoladeObjekat: factory, // Ensure this field is included
      brojSakupljenihBodova: 0,
      tipKupca: null
    };

    users.push(newUser);

    // Save new data to JSON file
    fs.writeFile('data/korisnici.json', JSON.stringify(users, null, 2), (err) => {
      if (err) return res.status(500).json({ message: 'Error saving user data' });

      // Create token for new user
      const token = jwt.sign({ id: newUser.id, username: newUser.korisnickoIme, role: newUser.uloga }, SECRET, { expiresIn: '1h' });

      res.json({ token, user: newUser });
    });
  });
});


module.exports = router;
