class Cart {
  constructor(data) {
    this.id = data.id;
    this.cokolade = data.cokolade || [];
    this.korisnik = data.korisnik;
    this.cena = data.cena;
    this.obrisano = data.obrisano;
  }
}

module.exports = Cart;
