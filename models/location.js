class Location {
  constructor(data) {
    this.id = data.id;
    this.geografskaDuzina = data.geografskaDuzina;
    this.geografskaSirina = data.geografskaSirina;
    this.adresa = data.adresa;
  }
}

module.exports = Location;
