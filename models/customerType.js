class CustomerType {
  constructor(data) {
    this.id = data.id;
    this.imeTipa = data.imeTipa;
    this.popust = data.popust;
    this.trazeniBrojBodova = data.trazeniBrojBodova;
  }
}

module.exports = CustomerType;
