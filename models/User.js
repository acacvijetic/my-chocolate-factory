class User {
  constructor(data) {
    this.id = data.id;
    this.korisnickoIme = data.korisnickoIme;
    this.lozinka = data.lozinka;
    this.ime = data.ime;
    this.prezime = data.prezime;
    this.pol = data.pol;
    this.datumRodjenja = data.datumRodjenja;
    this.uloga = data.uloga;
    this.sveKupovine = data.sveKupovine || [];
    this.korpa = data.korpa || [];
    this.fabrikaCokoladeObjekat = data.fabrikaCokoladeObjekat || null;
    this.brojSakupljenihBodova = data.brojSakupljenihBodova || 0;
    this.tipKupca = data.tipKupca || null;
  }
}

module.exports = User;
