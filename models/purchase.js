class Purchase {
  constructor(data) {
    this.id = data.id;
    this.cokolade = data.cokolade || [];
    this.fabrika = data.fabrika;
    this.datum_vreme = data.datum_vreme;
    this.cena = data.cena;
    this.kupac = data.kupac;
    this.status = data.status;
    this.obrisano = data.obrisano;
  }
}

module.exports = Purchase;
