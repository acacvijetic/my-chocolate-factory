class Chocolate {
  constructor(data) {
    this.id = data.id;
    this.naziv = data.naziv;
    this.cena = data.cena;
    this.vrsta = data.vrsta;
    this.fabrika = data.fabrika;
    this.tip = data.tip;
    this.gramaza = data.gramaza;
    this.opis = data.opis;
    this.slika = data.slika;
    this.status = data.status;
    this.kolicina = data.kolicina;
    this.obrisano = data.obrisano;
  }
}

module.exports = Chocolate;
