class Comment {
  constructor(data) {
    this.id = data.id;
    this.kupac = data.kupac;
    this.fabrika = data.fabrika;
    this.tekst = data.tekst;
    this.status = data.status;
    this.ocena = data.ocena;
  }
}

module.exports = Comment;
