class ChocolateFactory {
  constructor(data) {
    this.id = data.id;
    this.naziv = data.naziv;
    this.cokolade = data.cokolade || [];
    this.radnoVreme = data.radnoVreme;
    this.status = data.status;
    this.lokacija = data.lokacija;
    this.logo = data.logo;
    this.ocena = data.ocena;
  }
}

module.exports = ChocolateFactory;
