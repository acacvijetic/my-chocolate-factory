const ChocolateFactory = require('../models/chocolateFactory');
const ChocolateFactoryDao = require('../dao/chocolateFactoryDao');

class ChocolateFactoryController {
  static async createChocolateFactory(req, res) {
    try {
      // Dobavi sve fabrike čokolada
      const allFactories = await ChocolateFactoryDao.loadChocolateFactories();
      
      // Broj fabrika čokolada
      const factoryCount = allFactories.length;
      
      // Generiši novi ID
      const newId = (factoryCount + 1).toString();

      // Podaci o fabrici iz zahteva
      const chocolateFactoryData = req.body;

      // Dodaj novi ID u podatke o fabrici
      chocolateFactoryData.id = newId;

      // Napravi novu instancu fabrike čokolade
      const chocolateFactory = new ChocolateFactory(chocolateFactoryData);

      // Sačuvaj fabriku u bazi podataka
      await ChocolateFactoryDao.save(chocolateFactory);

      // Vrati kreiranu fabriku kao odgovor
      res.status(201).json(chocolateFactory);
    } catch (error) {
      console.error('Error creating chocolate factory:', error);
      res.status(500).json({ message: 'Error creating chocolate factory' });
    }
  }
  static async getAllChocolateFactories(req, res) {
    try {
      const chocolateFactories = await ChocolateFactoryDao.loadChocolateFactories();
      res.json(chocolateFactories);
    } catch (error) {
      console.error('Error fetching chocolate factories:', error);
      res.status(500).json({ message: 'Error fetching chocolate factories' });
    }
  }

  static async getChocolateFactory(req, res) {
    const id = req.params.id;
    const chocolateFactory = await ChocolateFactoryDao.findById(id);
    if (chocolateFactory) {
      console.log(chocolateFactory);
      res.json(chocolateFactory);
    } else {
      res.status(404).json({ message: 'Chocolate factory not found' });
    }
  }

  static async getChocolateFactoryByName(req, res) {
    const name = req.params.naziv;
    const chocolateFactory = await ChocolateFactoryDao.findByName(name);
    if (chocolateFactory) {
      console.log(chocolateFactory);
      res.json(chocolateFactory);
    } else {
      res.status(404).json({ message: 'Chocolate factory not found' });
    }
  }

  static async deleteChocolateFactory(req, res) {
    const id = req.params.id;
    await ChocolateFactoryDao.deleteById(id);
    res.status(204).send();
  }

  static async updateChocolateFactory(req, res) {
    const id = req.params.id;
    let chocolateFactory = await ChocolateFactoryDao.findById(id);
    if (chocolateFactory) {
      Object.assign(chocolateFactory, req.body);
      await ChocolateFactoryDao.save(chocolateFactory);
      res.json(chocolateFactory);
    } else {
      res.status(404).json({ message: 'Chocolate factory not found' });
    }
  }
}

module.exports = ChocolateFactoryController;
