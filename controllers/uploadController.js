// controllers/uploadController.js
const multer = require('multer');
const path = require('path');

// Konfiguracija Multer-a
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'slike'); // folder gde će slike biti sačuvane
  },
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}-${file.originalname}`); // generiši jedinstveno ime za fajl
  }
});

const upload = multer({ storage });

const uploadImage = (req, res) => {
  try {
    res.status(200).json({ imageUrl: `http://localhost:3000/slike/${req.file.filename}` });
  } catch (error) {
    console.error('Error uploading image:', error);
    res.status(500).json({ message: 'Error uploading image' });
  }
};

module.exports = {
  upload,
  uploadImage
};
