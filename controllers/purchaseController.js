const Purchase = require('../models/purchase');
const PurchaseDao = require('../dao/purchaseDao');

class PurchaseController {
  static async createPurchase(req, res) {
    try {
      // Dobavi sve korpe
      const allPurchases = await PurchaseDao.loadPurchases();
      
      // Broj korpi
      const purchaseCount = allPurchases.length;
      
      // Generiši novi ID
      const newId = (purchaseCount + 1).toString();
  
      // Podaci o korpi iz zahteva
      const purchaseData = req.body;
  
      // Dodaj novi ID u podatke o korpi
      purchaseData.id = newId;
  
      // Napravi novu instancu korpe
      const purchase = new Purchase(purchaseData);
  
      // Sačuvaj korpu u bazi podataka
      await PurchaseDao.save(purchase);
  
      // Vrati kreiranu korpu kao odgovor
      res.status(201).json(purchase);
    } catch (error) {
      console.error('Error creating purchase:', error);
      res.status(500).json({ message: 'Error creating purchase' });
    }
  }

  static async getPurchase(req, res) {
    const id = req.params.id;
    const purchase = await PurchaseDao.findById(id);
    if (purchase) {
      res.json(purchase);
    } else {
      res.status(404).json({ message: 'Purchase not found' });
    }
  }

  static async getAllPurchases(req, res) {
    try {
      const purchases = await PurchaseDao.loadPurchases();
      res.json(purchases);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  }
  
  static async deletePurchase(req, res) {
    const id = req.params.id;
    await PurchaseDao.deleteById(id);
    res.status(204).send();
  }

  static async updatePurchase(req, res) {
    const id = req.params.id;
    let purchase = await PurchaseDao.findById(id);
    if (purchase) {
      Object.assign(purchase, req.body);
      await PurchaseDao.save(purchase);
      res.json(purchase);
    } else {
      res.status(404).json({ message: 'Purchase not found' });
    }
  }
}

module.exports = PurchaseController;
