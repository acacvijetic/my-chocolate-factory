const Comment = require('../models/comment');
const CommentDao = require('../dao/commentDao');

class CommentController {
  static async createComment(req, res) {
    try {
      // Dobavi sve korpe
      const allComents = await CommentDao.loadComments();
      
      // Broj korpi
      const commentCount = allComents.length;
      
      // Generiši novi ID
      const newId = (commentCount + 1).toString();
  
      // Podaci o korpi iz zahteva
      const commentData = req.body;
  
      // Dodaj novi ID u podatke o korpi
      commentData.id = newId;
  
      // Napravi novu instancu korpe
      const comment = new Comment(commentData);
  
      // Sačuvaj korpu u bazi podataka
      await CommentDao.save(comment);
  
      // Vrati kreiranu korpu kao odgovor
      res.status(201).json(comment);
    } catch (error) {
      console.error('Error creating comment:', error);
      res.status(500).json({ message: 'Error creating comment' });
    }
  }

  static async getComment(req, res) {
    const id = req.params.id;
    const comment = await CommentDao.findById(id);
    if (comment) {
      res.json(comment);
    } else {
      res.status(404).json({ message: 'Comment not found' });
    }
  }

  static async deleteComment(req, res) {
    const id = req.params.id;
    await CommentDao.deleteById(id);
    res.status(204).send();
  }

  static async updateComment(req, res) {
    const id = req.params.id;
    let comment = await CommentDao.findById(id);
    if (comment) {
      Object.assign(comment, req.body);
      await CommentDao.save(comment);
      res.json(comment);
    } else {
      res.status(404).json({ message: 'Comment not found' });
    }
  }
  static async getCommentsByFactoryName(req, res) {
    const factoryName = req.params.fabrika;
    const comments = await CommentDao.findByFactoryName(factoryName);
    if (comments.length > 0) {
      res.json(comments);
    } else {
      res.status(404).json({ message: 'No comments found for this factory' });
    }
  }
  static async getAllComments(req, res) {
    const comments = await CommentDao.loadComments();
    res.json(comments);
  }
}

module.exports = CommentController;
