const Cart = require('../models/cart');
const CartDao = require('../dao/cartDao');

class CartController {
  static async createCart(req, res) {
    try {
      // Dobavi sve korpe
      const allCarts = await CartDao.loadCarts();
      
      // Broj korpi
      const cartCount = allCarts.length;
      
      // Generiši novi ID
      const newId = (cartCount + 1).toString();
  
      // Podaci o korpi iz zahteva
      const cartData = req.body;
  
      // Dodaj novi ID u podatke o korpi
      cartData.id = newId;
  
      // Napravi novu instancu korpe
      const cart = new Cart(cartData);
  
      // Sačuvaj korpu u bazi podataka
      await CartDao.save(cart);
  
      // Vrati kreiranu korpu kao odgovor
      res.status(201).json(cart);
    } catch (error) {
      console.error('Error creating cart:', error);
      res.status(500).json({ message: 'Error creating cart' });
    }
  }
  

  static async getCart(req, res) {
    const id = req.params.id;
    const cart = await CartDao.findById(id);
    if (cart) {
      res.json(cart);
    } else {
      res.status(404).json({ message: 'Cart not found' });
    }
  }

  static async getCartByCustomer(req, res) {
    const username = req.params.username;
    const carts = await CartDao.findByCustomer(username);
    if (carts.length > 0) {
      res.json(carts);
    } else {
      res.status(404).json({ message: 'Carts not found' });
    }
  }

  static async deleteCart(req, res) {
    const id = req.params.id;
    await CartDao.deleteById(id);
    res.status(204).send();
  }

  static async updateCart(req, res) {
    const id = req.params.id;
    let cart = await CartDao.findById(id);
    if (cart) {
      Object.assign(cart, req.body);
      await CartDao.save(cart);
      res.json(cart);
    } else {
      res.status(404).json({ message: 'Cart not found' });
    }
  }
}

module.exports = CartController;
