const Chocolate = require('../models/chocolate');
const ChocolateDao = require('../dao/chocolateDao');

class ChocolateController {
  static async createChocolate(req, res) {
    const chocolateData = req.body;
    const chocolate = new Chocolate(chocolateData);
    await ChocolateDao.save(chocolate);
    res.status(201).json(chocolate);
  }

  static async getChocolate(req, res) {
    const id = req.params.id;
    const chocolate = await ChocolateDao.findById(id);
    if (chocolate) {
      res.json(chocolate);
    } else {
      res.status(404).json({ message: 'Chocolate not found' });
    }
  }
  static async getAllChocolates(req, res) {
    try {
      const chocolates = await ChocolateDao.loadChocolates();
      res.json(chocolates);
    } catch (error) {
      console.error('Error fetching chocolates:', error);
      res.status(500).json({ message: 'Error fetching chocolates' });
    }
  }

  static async deleteChocolate(req, res) {
    const id = req.params.id;
    await ChocolateDao.deleteById(id);
    res.status(204).send();
  }

  static async updateChocolate(req, res) {
    const id = req.params.id;
    let chocolate = await ChocolateDao.findById(id);
    if (chocolate) {
      Object.assign(chocolate, req.body);
      await ChocolateDao.save(chocolate);
      res.json(chocolate);
    } else {
      res.status(404).json({ message: 'Chocolate not found' });
    }
  }
}

module.exports = ChocolateController;
