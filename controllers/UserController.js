const User = require('../models/User');
const UserDao = require('../dao/UserDAO');
const fs = require('fs');


class UserController {
  static async createUser(req, res) {
    try {
      const userData = req.body;

      // Dobavljanje svih korisnika iz JSON datoteke
      const allUsers = JSON.parse(fs.readFileSync('data/korisnici.json', 'utf8'));

      // Izbroji koliko korisnika ima
      const userCount = allUsers.length;

      // Generisanje novog ID-a za korisnika
      const newUserId = (userCount + 1).toString();

      // Postavljanje novog ID-a korisnika
      userData.id = newUserId;

      // Dodavanje novog korisnika u listu svih korisnika
      allUsers.push(userData);

      // Čuvanje ažurirane liste korisnika u JSON datoteku
      fs.writeFileSync('data/korisnici.json', JSON.stringify(allUsers, null, 2));

      // Vraćanje novog korisnika kao odgovor
      res.status(201).json(userData);
    } catch (error) {
      console.error('Error creating user:', error);
      res.status(500).json({ message: 'Internal server error' });
    }
  }

  static async getUser(req, res) {
    const korisnickoIme = req.params.korisnickoIme;
    const user = await UserDao.findByUsername(korisnickoIme);
    if (user) {
      res.json(user);
    } else {
      res.status(404).json({ message: 'User not found' });
    }
  }

  static async getAllUsers(req, res) {
    try {
      // Učitavanje svih korisnika iz datoteke
      const allUsers = await UserDao.loadUsers();
      // Slanje svih korisnika kao odgovor
      res.json(allUsers);
    } catch (error) {
      console.error('Error fetching all users:', error);
      res.status(500).json({ message: 'Internal server error' });
    }
  }

  static async deleteUser(req, res) {
    const korisnickoIme = req.params.korisnickoIme;
    await UserDao.deleteByUsername(korisnickoIme);
    res.status(204).send();
  }

  static async updateUser(req, res) {
    const korisnickoIme = req.params.korisnickoIme;
    let user = await UserDao.findByUsername(korisnickoIme);
    if (user) {
      Object.assign(user, req.body);
      await UserDao.save(user);
      res.json(user);
    } else {
      res.status(404).json({ message: 'User not found' });
    }
  }
}

module.exports = UserController;
