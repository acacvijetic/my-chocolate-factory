const Location = require('../models/location');
const LocationDao = require('../dao/locationDao');

class LocationController {
  static async createLocation(req, res) {
    const locationData = req.body;
    const location = new Location(locationData);
    await LocationDao.save(location);
    res.status(201).json(location);
  }

  static async getLocation(req, res) {
    const id = req.params.id;
    const location = await LocationDao.findById(id);
    if (location) {
      res.json(location);
    } else {
      res.status(404).json({ message: 'Location not found' });
    }
  }

  static async deleteLocation(req, res) {
    const id = req.params.id;
    await LocationDao.deleteById(id);
    res.status(204).send();
  }

  static async updateLocation(req, res) {
    const id = req.params.id;
    let location = await LocationDao.findById(id);
    if (location) {
      Object.assign(location, req.body);
      await LocationDao.save(location);
      res.json(location);
    } else {
      res.status(404).json({ message: 'Location not found' });
    }
  }
}

module.exports = LocationController;
