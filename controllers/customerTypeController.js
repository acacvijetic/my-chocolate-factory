const CustomerType = require('../models/customerType');
const CustomerTypeDao = require('../dao/customerTypeDao');

class CustomerTypeController {
  static async createCustomerType(req, res) {
    const customerTypeData = req.body;
    const customerType = new CustomerType(customerTypeData);
    await CustomerTypeDao.save(customerType);
    res.status(201).json(customerType);
  }

  static async getCustomerType(req, res) {
    const id = req.params.id;
    const customerType = await CustomerTypeDao.findById(id);
    if (customerType) {
      res.json(customerType);
    } else {
      res.status(404).json({ message: 'Customer type not found' });
    }
  }

  static async deleteCustomerType(req, res) {
    const id = req.params.id;
    await CustomerTypeDao.deleteById(id);
    res.status(204).send();
  }

  static async updateCustomerType(req, res) {
    const id = req.params.id;
    let customerType = await CustomerTypeDao.findById(id);
    if (customerType) {
      Object.assign(customerType, req.body);
      await CustomerTypeDao.save(customerType);
      res.json(customerType);
    } else {
      res.status(404).json({ message: 'Customer type not found' });
    }
  }
}

module.exports = CustomerTypeController;
