import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';
import FactoryDetails from '../components/FactoryDetails.vue';
import AddChocolate from '../components/AddChocolate.vue';
import EditChocolate from '../components/EditChocolate.vue';
import Comments from '../components/Comments.vue';
import AdminDashboard from '../views/Administrator/AdminDashboard.vue';
import AddFactory from '../views/Administrator/AddFactory.vue';
import RegisterManager from '../views/Administrator/RegisterManager.vue';
import ManagerDashboard from '../views/Manager/ManagerDashboard.vue';
import RegisterWorker from '@/views/Manager/RegisterWorker.vue';
import WorkerDashboard from '@/views/Worker/WorkerDashboard.vue';
import CustomerDashboard from '@/views/Customer/CustomerDashboard.vue';
import CustomerPurchases from '@/views/Customer/CustomerPurchases.vue';
import ManagerPurchases from '@/views/Manager/ManagerPurchases.vue';
import ViewCarts from '../components/Carts.vue';
import UserProfile from '../components/UserProfile.vue';
import AllUsers from '../views/Administrator/AllUsers.vue';

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/AboutView.vue')
  },
  {
    path: '/factory/:id',
    name: 'factory-details',
    component: FactoryDetails,
    props: true
  },
  {
    path: '/add-chocolate/:factoryName/:factoryId',
    name: 'add-chocolate',
    component: AddChocolate,
    props: true
  },
  {
    path: '/edit-chocolate/:factoryName/:factoryId/:chocolateId',
    name: 'edit-chocolate',
    component: EditChocolate,
    props: true
  },
  {
    path: '/comments/:factoryName',
    name: 'comments',
    component: Comments
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../components/Register.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../components/Login.vue')
  },
  {
    path: '/admin-dashboard',
    name: 'AdminDashboard',
    component: AdminDashboard,
    meta: { requiresAuth: true, role: 'Administrator' }
  },
  {
    path: '/manager-dashboard',
    name: 'ManagerDashboard',
    component: ManagerDashboard,
    meta: { requiresAuth: true, role: 'Menadzer' }
  },
  {
    path: '/worker-dashboard',
    name: 'WorkerDashboard',
    component: WorkerDashboard,
    meta: { requiresAuth: true, role: 'Radnik'}
  },
  {
    path: '/customer-dashboard',
    name: 'CustomerDashboard',
    component: CustomerDashboard,
    meta: { requiresAuth: true, role: 'Kupac'}
  },
  {
    path: '/add-factory',
    name: 'add-factory',
    component: AddFactory,
    meta: { requiresAuth: true, role: 'Administrator' }
  },
  {
    path: '/view-carts/:factoryName',
    name: 'view-carts',
    component: ViewCarts,
    //meta: { requiresAuth: true, role: 'Kupac' }
  },
  {
    path: '/register-manager',
    name: 'register-manager',
    component: RegisterManager,
    meta: { requiresAuth: true, role: 'Administrator' }
  },
  {
    path: '/register-worker/:factoryName',
    name: 'register-worker',
    component: RegisterWorker,
    meta: {requiresAuth: true, role: 'Menadzer'}
  },
  {
    path: '/customer-purchases',
    name: 'customer-purchases',
    component: CustomerPurchases,
    meta: {requiresAuth: true, role: 'Kupac'}
  },
  {
    path: '/manager-purchases',
    name: 'manager-purchases',
    component: ManagerPurchases,
    meta: {requiresAuth: true, role: 'Menadzer'}
  },
  {
    path: '/user-profile',
    name: 'user-profile',
    component: UserProfile,
    //meta: {requiresAuth: true, role: 'Menadzer'}
  },
  {
    path: '/all-users',
    name: 'all-users',
    component: AllUsers,
    //meta: {requiresAuth: true, role: 'Menadzer'}
  }
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
});

router.beforeEach((to, from, next) => {
  const token = localStorage.getItem('token');
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  const userRole = token ? JSON.parse(atob(token.split('.')[1])).role : null;

  if (requiresAuth && !token) {
    next({ name: 'login' });
  } else if (requiresAuth && to.meta.role !== userRole) {
    next({ name: 'home' });
  } else {
    next();
  }
});

export default router;
