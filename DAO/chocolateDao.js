const fs = require('fs');
const path = require('path');
const Chocolate = require('../models/chocolate');

class ChocolateDao {
  static getFilePath() {
    return path.join(__dirname, '../data/cokolade.json');
    }

  static async loadChocolates() {
    const filePath = this.getFilePath();
    if (!fs.existsSync(filePath)) {
      return [];
    }
    const data = await fs.promises.readFile(filePath, 'utf8');
    return JSON.parse(data).map(chocolate => new Chocolate(chocolate));
  }

  static async saveChocolates(chocolates) {
    const filePath = this.getFilePath();
    await fs.promises.writeFile(filePath, JSON.stringify(chocolates, null, 2));
  }

  static async findById(id) {
    const chocolates = await this.loadChocolates();
    return chocolates.find(chocolate => chocolate.id === id);
  }

  static async save(chocolate) {
    const chocolates = await this.loadChocolates();
    const existingIndex = chocolates.findIndex(c => c.id === chocolate.id);
    if (existingIndex !== -1) {
      chocolates[existingIndex] = chocolate;
    } else {
      chocolates.push(chocolate);
    }
    await this.saveChocolates(chocolates);
  }

  static async deleteById(id) {
    let chocolates = await this.loadChocolates();
    chocolates = chocolates.filter(chocolate => chocolate.id !== id);
    await this.saveChocolates(chocolates);
  }
}

module.exports = ChocolateDao;
