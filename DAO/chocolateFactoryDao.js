const fs = require('fs');
const path = require('path');
const ChocolateFactory = require('../models/chocolateFactory');

class ChocolateFactoryDao {
  static getFilePath() {
    return path.join(__dirname, '../data/fabrikeCokolade.json');
  }

  static async loadChocolateFactories() {
    const filePath = this.getFilePath();
    console.log(filePath);
    if (!fs.existsSync(filePath)) {
      return [];
    }
    const data = await fs.promises.readFile(filePath, 'utf8');
    return JSON.parse(data).map(chocolateFactory => new ChocolateFactory(chocolateFactory));
  }

  static async saveChocolateFactories(chocolateFactories) {
    const filePath = this.getFilePath();
    await fs.promises.writeFile(filePath, JSON.stringify(chocolateFactories, null, 2));
  }

  static async findById(id) {
    const chocolateFactories = await this.loadChocolateFactories();
    console.log(chocolateFactories);
    return chocolateFactories.find(chocolateFactory => (chocolateFactory.id) === (id));
  }

  static async findByName(name) {
    const chocolateFactories = await this.loadChocolateFactories();
    console.log(chocolateFactories);
    return chocolateFactories.find(chocolateFactory => (chocolateFactory.naziv) === (name));
  }

  static async save(chocolateFactory) {
    const chocolateFactories = await this.loadChocolateFactories();
    const existingIndex = chocolateFactories.findIndex(cf => cf.id === chocolateFactory.id);
    if (existingIndex !== -1) {
      chocolateFactories[existingIndex] = chocolateFactory;
    } else {
      chocolateFactories.push(chocolateFactory);
    }
    await this.saveChocolateFactories(chocolateFactories);
  }

  static async deleteById(id) {
    let chocolateFactories = await this.loadChocolateFactories();
    chocolateFactories = chocolateFactories.filter(chocolateFactory => chocolateFactory.id !== id);
    await this.saveChocolateFactories(chocolateFactories);
  }
}

module.exports = ChocolateFactoryDao;
