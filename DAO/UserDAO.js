const fs = require('fs');
const path = require('path');
const User = require('../models/User');

class UserDao {
  static getFilePath() {
    return path.join(__dirname, '../data/korisnici.json');  }

  static async loadUsers() {
    const filePath = this.getFilePath();
    if (!fs.existsSync(filePath)) {
      return [];
    }
    const data = await fs.promises.readFile(filePath, 'utf8');
    return JSON.parse(data).map(user => new User(user));
  }

  static async saveUsers(users) {
    const filePath = this.getFilePath();
    await fs.promises.writeFile(filePath, JSON.stringify(users, null, 2));
  }

  static async findByUsername(korisnickoIme) {
    const users = await this.loadUsers();
    return users.find(user => user.korisnickoIme === korisnickoIme);
  }

  static async findById(id) {
    const users = await this.loadUsers();
    return users.find(user => user.id === id);
  }

  static async save(user) {
    const users = await this.loadUsers();
    const existingIndex = users.findIndex(u => u.id === user.id);
    if (existingIndex !== -1) {
      users[existingIndex] = user;
    } else {
      users.push(user);
    }
    await this.saveUsers(users);
  }

  static async deleteByUsername(korisnickoIme) {
    let users = await this.loadUsers();
    users = users.filter(user => user.korisnickoIme !== korisnickoIme);
    await this.saveUsers(users);
  }
}

module.exports = UserDao;
