const fs = require('fs');
const path = require('path');
const CustomerType = require('../models/customerType');

class CustomerTypeDao {
  static getFilePath() {
    return path.join(__dirname, '../data/tipoviKupaca.json');
    }

  static async loadCustomerTypes() {
    const filePath = this.getFilePath();
    if (!fs.existsSync(filePath)) {
      return [];
    }
    const data = await fs.promises.readFile(filePath, 'utf8');
    return JSON.parse(data).map(customerType => new CustomerType(customerType));
  }

  static async saveCustomerTypes(customerTypes) {
    const filePath = this.getFilePath();
    await fs.promises.writeFile(filePath, JSON.stringify(customerTypes, null, 2));
  }

  static async findById(id) {
    const customerTypes = await this.loadCustomerTypes();
    return customerTypes.find(customerType => customerType.id === id);
  }

  static async save(customerType) {
    const customerTypes = await this.loadCustomerTypes();
    const existingIndex = customerTypes.findIndex(ct => ct.id === customerType.id);
    if (existingIndex !== -1) {
      customerTypes[existingIndex] = customerType;
    } else {
      customerTypes.push(customerType);
    }
    await this.saveCustomerTypes(customerTypes);
  }

  static async deleteById(id) {
    let customerTypes = await this.loadCustomerTypes();
    customerTypes = customerTypes.filter(customerType => customerType.id !== id);
    await this.saveCustomerTypes(customerTypes);
  }
}

module.exports = CustomerTypeDao;
