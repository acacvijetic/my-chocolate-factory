const fs = require('fs');
const path = require('path');
const Cart = require('../models/cart');

class CartDao {
  static getFilePath() {
    return path.join(__dirname, '../data/korpe.json');
    }

  static async loadCarts() {
    const filePath = this.getFilePath();
    if (!fs.existsSync(filePath)) {
      return [];
    }
    const data = await fs.promises.readFile(filePath, 'utf8');
    return JSON.parse(data).map(cart => new Cart(cart));
  }

  static async saveCarts(carts) {
    const filePath = this.getFilePath();
    await fs.promises.writeFile(filePath, JSON.stringify(carts, null, 2));
  }

  static async findById(id) {
    const carts = await this.loadCarts();
    return carts.find(cart => cart.id === id);
  }

  static async findByCustomer(username) {
    const carts = await this.loadCarts();
    return carts.filter(cart => cart.korisnik === username);
  }

  static async save(cart) {
    const carts = await this.loadCarts();
    const existingIndex = carts.findIndex(c => c.id === cart.id);
    if (existingIndex !== -1) {
      carts[existingIndex] = cart;
    } else {
      carts.push(cart);
    }
    await this.saveCarts(carts);
  }

  static async deleteById(id) {
    let carts = await this.loadCarts();
    carts = carts.filter(cart => cart.id !== id);
    await this.saveCarts(carts);
  }
}

module.exports = CartDao;
