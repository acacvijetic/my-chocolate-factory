const fs = require('fs');
const path = require('path');
const Location = require('../models/location');

class LocationDao {
  static getFilePath() {
    return path.join(__dirname, '../data/lokacije.json');
    }

  static async loadLocations() {
    const filePath = this.getFilePath();
    if (!fs.existsSync(filePath)) {
      return [];
    }
    const data = await fs.promises.readFile(filePath, 'utf8');
    return JSON.parse(data).map(location => new Location(location));
  }

  static async saveLocations(locations) {
    const filePath = this.getFilePath();
    await fs.promises.writeFile(filePath, JSON.stringify(locations, null, 2));
  }

  static async findById(id) {
    const locations = await this.loadLocations();
    return locations.find(location => location.id === id);
  }

  static async save(location) {
    const locations = await this.loadLocations();
    const existingIndex = locations.findIndex(l => l.id === location.id);
    if (existingIndex !== -1) {
      locations[existingIndex] = location;
    } else {
      locations.push(location);
    }
    await this.saveLocations(locations);
  }

  static async deleteById(id) {
    let locations = await this.loadLocations();
    locations = locations.filter(location => location.id !== id);
    await this.saveLocations(locations);
  }
}

module.exports = LocationDao;
