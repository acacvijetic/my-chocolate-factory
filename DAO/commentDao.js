const fs = require('fs');
const path = require('path');
const Comment = require('../models/comment');

class CommentDao {
  static getFilePath() {
    return path.join(__dirname, '../data/komentari.json');
    }

  static async loadComments() {
    const filePath = this.getFilePath();
    if (!fs.existsSync(filePath)) {
      return [];
    }
    const data = await fs.promises.readFile(filePath, 'utf8');
    return JSON.parse(data).map(comment => new Comment(comment));
  }

  static async saveComments(comments) {
    const filePath = this.getFilePath();
    await fs.promises.writeFile(filePath, JSON.stringify(comments, null, 2));
  }

  static async findById(id) {
    const comments = await this.loadComments();
    return comments.find(comment => comment.id === id);
  }

  static async save(comment) {
    const comments = await this.loadComments();
    const existingIndex = comments.findIndex(c => c.id === comment.id);
    if (existingIndex !== -1) {
      comments[existingIndex] = comment;
    } else {
      comments.push(comment);
    }
    await this.saveComments(comments);
  }

  static async deleteById(id) {
    let comments = await this.loadComments();
    comments = comments.filter(comment => comment.id !== id);
    await this.saveComments(comments);
  }
  
  static async findByFactoryName(factoryName) {
    const comments = await this.loadComments();
    return comments.filter(comment => comment.fabrika === factoryName);
  }
}

module.exports = CommentDao;
