const fs = require('fs');
const path = require('path');
const Purchase = require('../models/purchase');

class PurchaseDao {
  static getFilePath() {
    return path.join(__dirname, '../data/kupovine.json');
  }

  static async loadPurchases() {
    const filePath = this.getFilePath();
    if (!fs.existsSync(filePath)) {
      return [];
    }
    const data = await fs.promises.readFile(filePath, 'utf8');
    return JSON.parse(data).map(purchase => new Purchase(purchase));
  }

  static async savePurchases(purchases) {
    const filePath = this.getFilePath();
    await fs.promises.writeFile(filePath, JSON.stringify(purchases, null, 2));
  }

  static async findById(id) {
    const purchases = await this.loadPurchases();
    return purchases.find(purchase => purchase.id === id);
  }

  static async save(purchase) {
    const purchases = await this.loadPurchases();
    const existingIndex = purchases.findIndex(p => p.id === purchase.id);
    if (existingIndex !== -1) {
      purchases[existingIndex] = purchase;
    } else {
      purchases.push(purchase);
    }
    await this.savePurchases(purchases);
  }

  static async deleteById(id) {
    let purchases = await this.loadPurchases();
    purchases = purchases.filter(purchase => purchase.id !== id);
    await this.savePurchases(purchases);
  }
}

module.exports = PurchaseDao;
