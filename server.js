const express = require('express');
const cors = require('cors');
const jwt = require('jsonwebtoken');
const userRoutes = require('./routes/UserRoutes');
const customerTypeRoutes = require('./routes/customerTypeRoutes');
const locationRoutes = require('./routes/locationRoutes');
const chocolateFactoryRoutes = require('./routes/chocolateFactoryRoutes');
const chocolateRoutes = require('./routes/chocolateRoutes');
const cartRoutes = require('./routes/cartRoutes');
const purchaseRoutes = require('./routes/purchaseRoutes');
const commentRoutes = require('./routes/commentRoutes');
const path = require('path');
const uploadRoutes = require('./routes/uploadRoutes');


const app = express();
const SECRET = 'tajna'; 

app.use(express.json());
app.use('/slike', express.static(path.join(__dirname, '/slike')));
app.use(cors());

app.use('/api', uploadRoutes);

app.use('/api', userRoutes);
app.use('/api', customerTypeRoutes);
app.use('/api', locationRoutes);
app.use('/api', chocolateFactoryRoutes);
app.use('/api', chocolateRoutes);
app.use('/api', cartRoutes);
app.use('/api', purchaseRoutes);
app.use('/api', commentRoutes);

// Middleware za zaštitu ruta
const authenticate = (req, res, next) => {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if (!token) return res.status(401).send('Access Denied');

    try {
        const verified = jwt.verify(token, SECRET);
        req.user = verified;
        next();
    } catch (error) {
        res.status(400).send('Invalid Token');
    }
};

// Primer zaštićene rute
app.get('/api/protected', authenticate, (req, res) => {
    res.send('This is a protected route');
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
